package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

type ZarionioTabla struct {
	Items       []ZaronioTableItem `json:"items"`
	LastUpdated time.Time          `json:"last_updated"`
}

type ZaronioTableItem struct {
	PositionX int    `json:"position_x"`
	PositionY int    `json:"position_y"`
	Id        string `json:"id"`
}

var tabla = ZarionioTabla{}

var defaultItems = []string{
	"Kuki",
	"Manca",
	"Gaco",
	"Lomen",
	"Ines",
	"Tišti",
	"Brina",
	"Seljo",
	"Rozamunda",
	"Roba",
	"Urška",
	"Jackie",
	"Klemzo",
	"Maja",
	"Matuš",
	"Kakarina",
	"Kirbiš",
	"Kocbek",
	"Gloria",
	"Bina",
	"Ana",
}

var openConnections = sync.Map{}

func main() {

	for i := range defaultItems {
		tabla.Items = append(tabla.Items, ZaronioTableItem{
			PositionX: 0,
			PositionY: 0,
			Id:        defaultItems[i],
		})
	}

	tabla.LastUpdated = time.Now()

	fmt.Println("Starting API")
	http.HandleFunc("/ws", wsEndpoint)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	// upgrade this connection to a WebSocket
	// connection
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
	}

	p, err := json.Marshal(tabla)

	err = ws.WriteMessage(1, p)
	if err != nil {
		log.Println(err)
	}
	// listen indefinitely for new messages coming
	// through on our WebSocket connection
	connectionId := rand.Uint64()
	fmt.Println("New connection: ", connectionId)
	openConnections.Store(connectionId, ws)
	reader(connectionId, ws)
}

func reader(mapKey uint64, conn *websocket.Conn) {
	for {
		// read in a message
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			openConnections.Delete(mapKey)
			log.Println(err)
			return
		}
		// print out that message for clarity
		log.Println(string(p))
		if string(p) != "" {
			err = json.Unmarshal(p, &tabla)
			if err != nil {
				log.Println(err)
				continue
			}

			tabla.LastUpdated = time.Now()
			p, err = json.Marshal(tabla)
			if err != nil {
				log.Println(err)
				continue
			}
			openConnections.Range(func(key, value interface{}) bool {
				if key == mapKey {
					return true
				}
				if err := value.(*websocket.Conn).WriteMessage(messageType, p); err != nil {
					log.Println(err)
					openConnections.Delete(key)
					err = nil
				}
				return true
			})
		}

		p, err = json.Marshal(tabla)
		if err != nil {
			log.Println(err)
			continue
		}

		if err := conn.WriteMessage(messageType, p); err != nil {
			openConnections.Delete(mapKey)
			return
		}

	}
}
