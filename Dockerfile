FROM alpine:3.18

RUN apk add --no-cache tzdata

COPY ZaronioTabla_API /opt/ZaronioTabla_API

CMD ["/opt/ZaronioTabla_API"]